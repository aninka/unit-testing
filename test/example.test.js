const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // initialization
        // create objects.. etc
        console.log("Initialising tests.")
    });
    it("Can add 1 and 2 together", () => {
        // test - adding
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3 for some reason?");
    });
    it("Cannot divide by 0", () => {
        // test - dividing
        expect(mylib.divide(8,0)).equal(Error, "Can divide with 0 for some reason?");
    }),
    it("Can subtract 10 and 5", () => {
        // test - subtracting
        expect(mylib.subtract(10,5)).equal(5, "10 - 5 is not 5 for some reason?");
    }),
    it("Can multiply 7 with 0", () => {
        // test - multiplying
        expect(mylib.multiply(7,0)).equal(0, "7 * 0 is not 0 for some reason?");
    })
    after(() => {
        // clean up
        // for example: shut down the Express server.
        console.log("Testing completed.")
    });
});