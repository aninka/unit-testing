/** Basic arithmetic operations */
const mylib = {
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    divide: (dividend, divisor) => {
        if (divisor === 0 || isNaN(divisor)) {
            return "Error";
        } else {
            return dividend / divisor;
        }
    },
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;